PACKAGES   = node_modules
TOOLS     := $(shell yarn bin)

.PHONY: help
help:
	@echo "Usage:"
	@echo "    help:           Prints this screen"
	@echo "    install-deps:   Installs dependencies"
	@echo "    update-images:  Updates the images"
	@echo "    build:          Build images"
	@echo "    init-db:        Initalize the speed-test and superset DB"
	@echo "    run:            Brings up superset"
	@echo "    run-speed-test: Runs an instance of the speed test and captures it in postgres"
	@echo "    clean:          Clean out temp space"
	@echo ""

$(PACKAGES):
	@echo "Installing local dependencies"
	yarn --ignore-platform install

.PHONY:
install-deps: $(PACKAGES)

.PHONY: update-images
update-images:
	@echo "Updating all base images"
	docker-compose build --pull

.PHONY: build
build:
	@echo "Building the production image"
	docker-compose build

.PHONY: init-db
init-db:
	docker-compose run superset superset-init

.PHONY: run
run: build
	@echo "Bringing up Superset"
	docker-compose up

.PHONY: run-speed-test
run-speed-test: $(PACKAGES)
	@echo "Running a speed test"
	yarn run start

.PHONY: clean
clean:
	@echo "Bringing down docker and removing temporary files"
	docker-compose down || true
	rm -rf $(PACKAGES)
